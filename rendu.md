# Rendu "Les droits d’accès dans les systèmes UNIX"

*Birlouez, Martin, martin.birlouez.etu@univ-lille.fr*  

## Question 1

Le procesus ne peut pas ouvrire le fichier car l'utilisateur toto na pas ce droit.  
Meme si son groupe lui posssède ce droit les droit user son supérieur aux droits group.   
--> Comande utiliser :  
> useradd toto # ajoute un utilisateur toto  
> user mod -a -G ubuntu toto # ajoute toto au groupe ubuntu  


## Question 2

Ici seul l'utilisateur ubuntu peut entré dans ce dossier.  
Les droits 'x' du groupe étant retiré toto ne peut y acceder.  
--> Comande utiliser :  
> ls la # lister les fichiers et donne des information cf man ls  
> mkdir mydir # créé un dossier 'mydir'  
> chmod og-x mydir # supprime les droit d'execution du groupe et de other sur mydir  

Lorsque nous fesont un *ls -l* sur mydir avec toto nous remarquons que les fichiers son 
lister mais que les informations sur les owner, les groups et les droit son masquer.  
> touch mydir/data.txt # créé un fichier data.txt dans mydir  

## Question 3

Avec l'utilisateur ubuntu les ids sont a 1000 et la lecture est possible.  
Avec l'utilisateur toto les ids sont a 1001 et la lecture est interdite.  

Nous effectuons en suite la commande *chmod u+s suid*  
et le fichier peut maintenant etre lu par toto. En effet son euid a changer, il 
vaut maintenant 1000.  

Cf script *./question3*  

## Question 4

Les ids resultants de l'execution de suid.py indique 1001 ce qui indique que ces 
le lanceur de la commande pyton3 qui defini les droits de cette commande.  

## Question 5

la commande chfn permet de modifier les information utilisateur contenue dans passwd.  
> chfn -r 5 toto # modifie par exemple le N de post de l'utilisateur toto.  

Nous notons que les droits de la commande shfn a le flag set-uid actif ce qui permet
 a toto de modifier ces informations en executant la commande.  

## Question 6

Les mots de passe utilisateur son stocker dans le fichier /etc/shadow
et son hasher en MD5(il me semble). Cela évite de stoker les mots de passe en claire
 dans le systeme.  

## Question 7

Cf scripts dans *./question7*    
Les groupes utiliser sont group_a, group_b, group_c  
Les utilisateur utiliser sont admin_g, lambda_a, lambda_b  
Les mots de passe de connection des utilisateurs respectif sont admin_g, lambda_a, lambda_b    

L'organisation de la Q7 est la suivante :    

question7/  
├── checkGrpA  
├── checkGrpAdm  
├── checkGrpB  
├── dir_a  
├── dir_b  
├── dir_c  
└── setupQuestion    

Avec le script 'setupQuestion' qui met tout en place et les scripts 'checkGrpA', 
'checkGrpB' et 'checkGrpAdm' qui verifie respectivement les droit du group_a, du
 group_b et de admin_g.  

Si nous observons les droits des dossier dir_a, dir_b et dir_c:    

drwxrws--T 2 admin_g group_a 4096 24 janv. 14:32 dir_a  
drwxrws--T 2 admin_g group_b 4096 24 janv. 14:32 dir_b  
drwxr-s--T 2 admin_g group_c 4096 24 janv. 14:32 dir_c    

Nous remarquons que le flag g+s et +t ont était ajouter afin de définir certaine régles
quand aux groupes des dossiers et des fichiers recursif, ou quand à la suppréssion et 
au renomage des fichiers.  

## Question 8

Cf scripts et README.txt dans *./question8*  
Dans cette question j'ai fait plusieur test (notament visant a éclairecir la Q3)  

## Question 9

Cf scripts dans *./question9*    
Afin de changer d'utilisateur nous utilisons la commande suivante:    
> su <nom_utilisateur>    
Cela nous permet de lancer les commande en temps que d'autre utilisateur.  

## Question 10

Cf scripts dans *./question10*  

