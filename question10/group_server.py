import os
import socket
import pwd
import crypt
from hmac import compare_digest as comph

def checklogin(login,mdp)-> bool:
	pwdf = "../question8/passwd"
	fkey = "Une legende raconte que le language C n'est pas si mal ... aprés ce n'est qu'une légende"
	fp = open(pwdf,'r')
	fpl = fp.readlines()
	passwd = {}
	for l in fpl :
		lp = l.split(":")
		passwd[lp[0]] = lp[1][:-1]
	mdph = crypt.crypt(mdp,fkey)
	return comph(mdph,passwd[login])


def mycat(path)-> str:
	cmd = f"cat {path}"
	rep = os.popen(cmd).read()
	return rep

def myls(path)-> str:
	cmd = f"ls {path}"
	rep = os.popen(cmd).read()
	return rep


HOST = "127.0.0.1"
PORT = 4000

print(f"Start serveur on {HOST}:{PORT}")

s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.bind((HOST,PORT))

while True:
	s.listen()
	conn,addr = s.accept()
	print(f"Server accept connection about {addr}")
	p = os.fork()
	if p > 0:
		#child
		cpid = os.getpid()
		print(f"{cpid}: start")
		with conn:
			print(f"{cpid}: from {addr}")
			#login
			msg = conn.recv(512)
			msg = msg.decode('utf-8')
			auth = checklogin(msg.split(',')[0],msg.split(',')[1])
			if not auth:
				print(f"{cpid}: auth err")
				conn.sendall(b"auth-ER")
				exit(1)
			else:
				conn.sendall(b"auth-OK")
				print(f"{cpid}: auth ok")
				login = msg.split(',')[0]
				uid = pwd.getpwnam(login).pw_uid
				print(f"{cpid}: I'am {login},{uid}")
				os.setuid(uid)
			while True:
				data = conn.recv(2048)
				if str(data) == "close":
					break
					exit(1)
				else:
					data = data.decode('utf-8')
					print(f"{cpid}-{login}: msg: {data}")
					if "cat " in data:
						path = data.split(' ')[1]
						rep = mycat(path)
						print(f"{cpid}-{login}: launch cat on {path}")
						conn.sendall(bytes(rep,'utf8'))
					elif "ls " in data:
						path = data.split(' ')[1]
						rep = myls(path)
						print(f"{cpid}-{login}: launch ls  on {path}")
						conn.sendall(bytes(rep,'utf8'))
					else:
						conn.sendall(b"msg recv Ok")
	else:
		#server continu
		print("Server wait a other connection")



























