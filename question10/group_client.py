import socket

HOST = "127.0.0.1"
PORT = 4000

s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.connect((HOST,PORT))

print("login:\n\t->", end='')
login = input()
print("mdp  :\n\t->", end='')
mdp = input()
msg = bytes(login+','+mdp,'utf-8')
s.sendall(msg)
res = s.recv(512)
res = res.decode('utf-8')
print(res)
if res != "auth-OK":
	print("-> Authentification Error")
	s.close()
	exit(1)
else:
	print("-> Authentification Check")

while True:
	print("Enter msg or commande:\n\t->",end='')
	txt = input()
	txtb = bytes(txt,'utf-8')
	s.sendall(txtb)
	data = s.recv(2048)
	data = data.decode('utf-8')
	print(f"-> {data}")
	if txt == "close":
		break

print("End client")
s.close()
