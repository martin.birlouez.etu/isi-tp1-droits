
import sys
import os
import pwd
from checkmdp import checkmdp as cmdp

try:
	f = sys.argv[1] if os.path.isfile(sys.argv[1]) else "null"
except OSError:
	print("Vous ne pouvez pas acceder a ce fichier")


if f == "null":
	print(f"{sys.argv[1]} is not file")
	exit(1)

# grab username & groupe

ruid = os.getresuid()
name = pwd.getpwuid(ruid[0]).pw_name
filegid = os.stat(f).st_gid

# parse /etc/group

try:
	fg = open("/etc/group",'r')
except OSError:
	print("fichier /etc/group introuvable")
	exit(2)

fgl = fg.readlines()
groups = {}
for l in fgl :
	lg = l.split(":")
	groups[lg[2]] = lg[3]+','+lg[0]

# check group

groupfileUsers = []
for u in groups[str(filegid)].split(','):
	groupfileUsers.append(u.replace("\n",""))


if name in groupfileUsers:
	print(name,groupfileUsers)
else:
	print(groupfileUsers)

# demande du mdp

print("Donnez le mot de pass pour supprimer le fichier")
mdp = input()

authOk = cmdp.checkmdpH(name,mdp)

if authOk :
	#os.remove(f)
	print(f"ficher {name} effacer")
else:
	print("Autentification echouer")
	










