#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

int main(int argc, char *argv[])
{

	//print ids

	uid_t euid;
	uid_t egid;
	uid_t ruid;
	uid_t rgid;

	euid = geteuid();
	egid = getgid();
	ruid = getuid();
	rgid = getegid();

	printf("euid: %u	egid: %u \n",euid,egid);
	printf("ruid: %u	rgid: %u \n",ruid,rgid);

	//print file
	
	FILE* f = NULL;
	int k   = 0;
	errno = 0;
	f = fopen(argv[1],"r");
	if (!errno){
		k = fgetc(f);
		while(k != EOF){
			printf("%c",k);
			k = fgetc(f);
		}
		fclose(f);
	}else{
		perror("Erreur de lecture du fichier ");
		return 1;
	}
	


    return 0;
}
